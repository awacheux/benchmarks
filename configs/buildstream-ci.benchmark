# Benchmarking configuration for BuildStream's standard CI performance test

# This was created just by removing the 'versions' section from the default
# benchmark configuration.

version_defaults:
  base_docker_image: docker.io/buildstream/buildstream-fedora
  buildstream_repo: https://gitlab.com/BuildStream/BuildStream

test_defaults:
  measurements_file: /root/measurements.json
  repeats: 3

tests:
- name: Startup time
  # FIXME: Rather than bodging /usr/bin/time to generate JSON, we should
  # have the 'bst-measure' log parser script take on this functionality.
  script: |
    /usr/bin/time \
        -o /root/measurements.json \
        -f '{ "total-time": %e, "max-rss-kb": %M }' \
        -- bst --help

- name: Build of Baserock stage1-binutils for x86_64
  description: |
    Build a real BuildStream project and measure total build time.

    We are using the Baserock "reference systems" project here mostly because
    it can still be built with BuildStream 1.0.0.
  mounts:
  - volume: baserock-source
    path: /src
  script: |
    git clone https://gitlab.com/BuildStream/bst-external/
    cd bst-external
    git checkout c4cc10467d116450471ca9f756617ede7572814c
    pip3 install --user .
    cd ..

    git clone https://gitlab.com/baserock/definitions
    cd definitions
    git checkout 9da3339c8b3d5080fcacc2f3f9a38178e3c9879c  # branch sam/bst-1.0.0, MR !76

    # Use pre-created source cache to avoid measuring the network fetch time.
    mkdir -p ~/.config
    echo "sourcedir: /src" > ~/.config/buildstream.conf

    # Disable the artifact cache to ensure a full build.
    sed -e '/artifacts:/,/^$/ d' -i project.conf

    # Run the build.
    # FIXME: again, the log parser script will allow for more detailed results
    /usr/bin/time \
        -o /root/measurements.json \
        -f '{ "total-time": %e, "max-rss-kb": %M }' \
        -- bst build gnu-toolchain/stage1-binutils.bst

    # Or, for a more complete test: bst build systems/minimal-system-image-x86_64.bst
  repeats: 3

# This kind of test will be implemented once the project generation tool exists.
#
#  - name: Benchmark 1
#    generate-project:
#      elements: [100, 1000, 10000]
#    only-measure-elements:
#    - first.bst
#    - last.bst

volumes:
- name: baserock-source
  description: |
    Volume holding all sources needed for the Baserock components that we build.
  prepare:
    version: version-under-test
    path: /src
    script: |
      git clone https://gitlab.com/BuildStream/bst-external/
      cd bst-external
      git checkout c4cc10467d116450471ca9f756617ede7572814c
      pip3 install --user .
      cd ..

      mkdir -p ~/.config
      echo "sourcedir: /src" > ~/.config/buildstream.conf

      git clone https://gitlab.com/baserock/definitions
      cd definitions
      git checkout 9da3339c8b3d5080fcacc2f3f9a38178e3c9879c  # branch sam/bst-1.0.0, MR !76

      bst fetch gnu-toolchain/stage1-binutils.bst

      # Or, for a more complete test: bst fetch systems/minimal-system-image-x86_64.bst
