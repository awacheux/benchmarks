import collections
import os
import psutil
import subprocess


# Returns a dict containing all of the collected host info
def get_host_info():
    return {
        'total_system_memory_GiB': get_memory_info(),
        'processor_info': get_processor(),
        'kernel_release': get_kernel_info()}


# This returns a number
def get_memory_info():
    return bytes_to_gib(psutil.virtual_memory().total)


# This argument is a number
# This returns a number
def bytes_to_gib(value):  # Bytes to Gibibytes
    return round(value / (1024 * 1024 * 1024), 2)


# This returns a utf-8 string
def get_kernel_info():
    cmd = ["uname", "-r"]
    output = subprocess.check_output(cmd)
    return str(output, encoding="utf-8").rstrip()


# This returns a string
def get_processor():
    cores = 0
    with open('/proc/cpuinfo') as f:
        for line in f:
            if line.strip():
                if line.rstrip('\n').startswith('model name'):
                    model_name = line.rstrip('\n').split(':')[1]
                    cores += 1
    # This returns the model name as a string
    # This is done as the name cannot be reliably parsed
    # Due to different vendors and models having different naming conventions
    return {"cpu_model_name": model_name, "cpu_cores": cores}
